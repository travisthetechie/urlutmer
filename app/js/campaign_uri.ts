/// <reference path="URI.d.ts" />
interface CampaignDescription {

}

class CampaignUri {
  source : string;
  medium : string;
  term : string;
  content : string;
  name : string;

  constructor(public baseUrl : string) {

  }

  getUrl() {
    var uri = new URI(this.baseUrl);

    if (this.source)
      uri.addQuery({utm_source: this.source});
    if (this.medium)
      uri.addQuery({utm_medium: this.medium});
    if (this.term)
      uri.addQuery({utm_term: this.term});
    if (this.content)
      uri.addQuery({utm_content: this.content});
    if (this.name)
      uri.addQuery({utm_campaign: this.name});

    return uri.normalize().toString();
  }
}