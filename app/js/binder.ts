/// <reference path="jquery.d.ts" />
/// <reference path="campaign_uri.ts" />
/// <reference path="form.ts" />

(() => {
  function buildCampaignUri() {
    var uri = new CampaignUri($("#baseUrl").val());

    uri.source  = Form.$source.val();
    uri.medium  = Form.$medium.val();
    uri.term    = Form.$term.val();
    uri.content = Form.$content.val();
    uri.name    = Form.$name.val();

    $("#result").val(uri.getUrl());
  }

  $("input[type=text]").on("keyup focus unfocus", buildCampaignUri)

  $("#result").on("click", (evt : JQueryEventObject) => {
    $(evt.target).focus();
    $(evt.target).select();
  });
})();
