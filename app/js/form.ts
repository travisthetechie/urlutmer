/// <reference path="jquery.d.ts" />

module Form {
  export var $source  = $("#source");
  export var $medium  = $("#medium");
  export var $term    = $("#term");
  export var $content = $("#content");
  export var $name    = $("#name");
}
