/// <reference path="form.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="URI.d.ts" />

(() => {
  var queryParams = new URI(document.URL).query(true);

  Form.$content.val(queryParams["content"]);
  Form.$medium.val(queryParams["medium"]);
  Form.$name.val(queryParams["name"]);
  Form.$source.val(queryParams["source"]);
  Form.$term.val(queryParams["term"]);

})();
