/// <reference path="jasmine.d.ts" />
/// <reference path="jasmine-given.d.ts" />
/// <reference path="../app/js/campaign_uri.ts" />

describe("campaign_uri", () => {
  var target:CampaignUri;
  var baseUrl:string = "https://www.google.com/";
  Given(() => target = new CampaignUri(baseUrl));

  describe("setting the source", () => {
    When(() => target.source = "the-source");
    Then(() => expect(target.getUrl()).toEqual(baseUrl + "?utm_source=the-source"));
  });

  describe("setting the medium", () => {
    When(() => target.medium = "hi-medium");
    Then(() => expect(target.getUrl()).toEqual(baseUrl + "?utm_medium=hi-medium"));
  });

  describe("setting the term", () => {
    When(() => target.term = "the-term");
    Then(() => expect(target.getUrl()).toEqual(baseUrl + "?utm_term=the-term"));
  });

  describe("setting the content", () => {
    When(() => target.content = "the-content");
    Then(() => expect(target.getUrl()).toEqual(baseUrl + "?utm_content=the-content"));
  });

  describe("setting the name", () => {
    When(() => target.name = "the-name");
    Then(() => expect(target.getUrl()).toEqual(baseUrl + "?utm_campaign=the-name"));
  });

  describe("setting them all", () => {
    When(() => target.source = "the-source");
    When(() => target.medium = "the-medium");
    When(() => target.term = "the-term");
    When(() => target.content = "the-content");
    When(() => target.name = "the-name");

    Then(() => expect(target.getUrl()).toEqual("https://www.google.com/?utm_source=the-source&utm_medium=the-medium&utm_term=the-term&utm_content=the-content&utm_campaign=the-name"));
  })
});